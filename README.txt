
Overview
--------

The ISP Login module lets users log in using any IMAP or POP3 email account. It
is mainly intended for ISPs which have lots of virtual domains per server, and
maybe different mail login methods on different servers.

The user must login using his complete e-mail address, in the form
user@mail_domain, and the corresponding password. Then the module tries to open
a POP3 or IMAP connection. If it succeeds, the user is allowed in, and a new
drupal account is created for him if necessary.

The module:
-----------

* Extracts the username and mail_domain from (Drupal) user login form.
  (the user must log in as username@mail_domain)
* Queries the DNS system to get the MX host designated for mail_domain.
  The returned value is the mail_host
* If no MX record is returned, then it assumes that mail_domain is a
  host (not a domain) and must be the mail_host itself
* If mail_host is still a DNS name, it is converted into the corresponding
  IP address using another DNS query.
* If mail_host is *not* listed on the ISP Login settings page the login is
  denied.
* If it is, the module tries to connect to mail_host using the method
  described there. The login is allowed if the connection succeeds, and
  denied otherwise.
* On the first login, a new drupal account is automatically created for
  the new user.

See the ISP Login settings page for more information about the available
mail server access methods.


Requirements
------------

* Drupal 5.x or higher
* Php IMAP extension installed
  See http://www.php.net/manual/en/ref.imap.php
* PHP 5 or higher for full functionality
  (for "did" mail server access mode only)


Install
-------

1. Copy the 'isp_login' module directory into the Drupal
   modules directory as usual

2. Activate the module via the Drupal module configuration menu:
   Administer >> Site building >> Modules

3. Configuration:
   Administer >> User management >> ISP Login


License
-------

GPL
